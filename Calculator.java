public class Calculator {
	public int addInputs(int inputOne, int inputTwo){
		return inputOne+inputTwo;
	}
	public int subtractInputs(int inputOne, int inputTwo){
		return inputOne-inputTwo;
	}
	public static int multiplyInputs(int inputOne, int inputTwo){
		return inputOne*inputTwo;
	}
	public static double divideInputs(double inputOne, double inputTwo){
		return inputOne/inputTwo;
	}
}