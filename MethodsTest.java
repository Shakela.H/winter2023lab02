public class MethodsTest{
	public static void main (String [] args){
		int x = 5;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(x+10);
		System.out.println(x);
		methodTwoInputNoReturn(1,1.0);
		int z = methodNoInputReturnInt();
		System.out.println("value of z, the variable containing the methodNoInputReturnInt: " + z);
		double y = sumSquareRoot(9,5);
		System.out.println("Square value: " + y);
		
		String s1 = "Java";
		String s2 = "Programming";
		System.out.println("This is s1 length: " + s1.length());
		System.out.println("This is s2 length: " + s2.length());
		
		System.out.println("this is from the SecondClass.addOne(): " + SecondClass.addOne(50));
		
		SecondClass sc = new SecondClass();
		System.out.println("this is from the sc.addTwo(): " + sc.addTwo(50));
	};
	
	public static void methodNoInputNoReturn(){
		System.out.println("I’m in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
	};
	
	public static void methodOneInputNoReturn(int input){
		input -=5;
		System.out.println("Inside the method one input no return. Input: " + input);
		
	};
	
	public static void methodTwoInputNoReturn ( int valueOne, double valueTwo){
		System.out.println("this is value 1: " + valueOne);
		System.out.println("this is value 2: " + valueTwo);
	};
	
	public static int  methodNoInputReturnInt(){
		return 5;
	}
	
	public static double sumSquareRoot(int firstValue, int secondValue){
		return Math.sqrt(firstValue + secondValue);
	};
};