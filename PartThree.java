import java.util.Scanner;

public class PartThree{
	public static void main (String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the first number:");
		int firstInput = sc.nextInt();
		System.out.println("Enter the second number:");
		int secondInput = sc.nextInt();
		
		Calculator calc = new Calculator();
		System.out.println("Addition of the two values: "+calc.addInputs(firstInput,secondInput));
		System.out.println("Substraction of the two values: "+calc.subtractInputs(firstInput,secondInput));
		System.out.println("Multiplication of the two values: "+ Calculator.multiplyInputs(firstInput,secondInput));
		System.out.println("Division of the two values: "+ Calculator.divideInputs(firstInput,secondInput));
	};
};